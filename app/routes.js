angular.module('acheApp.routes', [])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('root', {
                url: '',
                abstract:true,
                views:{
                    'root-view': {
                        templateUrl: 'modules/layout/layout.html',
                        controller: 'LayoutCtrl'
                    }
                }
            })
            .state('root.home', {
                url:'/',
                views:{
                    'content':{
                        templateUrl:'modules/home/home.html',
                        controller: 'HomeCtrl',
                        controllerAs: 'home'
                    }
                }
            });
        $urlRouterProvider.otherwise('/');
    })
    .run(function($rootScope){
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            if(toState.title){
                $rootScope.title = toState.title;
            }
        });
    });
