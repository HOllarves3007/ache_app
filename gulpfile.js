var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    nano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    sh = require('shelljs'),
    ngAnnotate = require('gulp-ng-annotate'),
    angularFilesort = require('gulp-angular-filesort'),
    inject = require('gulp-inject'),
    watch = require('gulp-watch'),
    filter = require('gulp-filter'),
    through = require('through2'),
    del = require('del'),
    shell = require('gulp-shell'),
    plumber = require('gulp-plumber'),
    server = require('gulp-server-livereload');

var paths = {
    sass: ['app/**/*.scss'],
    js: ['app/**/*.js'],
    assets: ['app/assets/**/*'],
    templates: ['app/**/*.html', '!./app/index.html'],
    settings: ['settings'],
    build: 'build'
};

gulp.task('default', ['sass', 'js']);

gulp.task('sass', function (done) {
    gulp.src('./app/app.scss')
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(gulp.dest('./build/css/'))
        .pipe(nano())
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest(paths.build + '/css/'))
        .on('end', done);
});

gulp.task('index', function () {
    gulp.src('./app/index.html')
        .pipe(
            inject(
                gulp.src(paths.js)
                    .pipe(plumber())
                    .pipe(angularFilesort()), {relative: true}
            )
        )
        .pipe(gulp.dest(paths.build));
});

function createCopyTasks(taskName, source, dest, customTaskCallback){
    function baseCopyTask(extendBaseTaskCallback){
        var myFilter = filter(function (file) {
            return file.event === 'unlink';
        });

        var baseTask = gulp.src(source);

        if(extendBaseTaskCallback){
            baseTask = extendBaseTaskCallback(baseTask);
        }

        if(customTaskCallback){
            baseTask = customTaskCallback(baseTask);
        }

        baseTask.pipe(gulp.dest(dest))
            .pipe(myFilter)
            .pipe(through.obj(function (chunk, enc, cb) {
                del(chunk.path);
                cb(null, chunk);
            }));
    }

    gulp.task(taskName, function(){
        baseCopyTask();
    });

    gulp.task(taskName + "-watch", function(){
        baseCopyTask(function(task){
            return task.pipe(watch(source));
        });
    });
}

createCopyTasks('js', paths.js, paths.build, function(task){
    return task
        .pipe(plumber())
        .pipe(ngAnnotate());
});
createCopyTasks('assets', paths.assets, paths.build + "/assets");
createCopyTasks('templates', paths.templates, paths.build);

gulp.task('build', ['sass', 'js', 'assets', 'templates', 'index']);

gulp.task('watch', ['js-watch', 'assets-watch', 'templates-watch'], function () {
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.js.concat(['./app/index.html']), ['index']);
});

gulp.task('install', shell.task(['bower install --allow-root']));

gulp.task('clean', function () {
    del.sync([paths.build + '/**', '!' + paths.build, '!' + paths.build + '/lib/**']);
});

gulp.task('env-dev', function () {
    gulp.src(paths.settings + '/settings.dev.js')
        .pipe(rename('settings.js'))
        .pipe(gulp.dest(paths.build));
});

gulp.task('env-staging', function () {
    gulp.src(paths.settings + '/settings.staging.js')
        .pipe(rename('settings.js'))
        .pipe(gulp.dest(paths.build));
});

gulp.task('env-production', function () {
    gulp.src(paths.settings + '/settings.production.js')
        .pipe(rename('settings.js'))
        .pipe(gulp.dest(paths.build));
});

gulp.task('serve', ['build', 'watch'], function () {
    gulp.src(paths.build)
        .pipe(server({
            host: '0.0.0.0',
            livereload: {
                enable: true,
                port: 8006
            },
            directoryListing: false,
            open: false
        }));
});
